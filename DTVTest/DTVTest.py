from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import sys, serialTLC, time
from testUI import Ui_DTVTestForm
 
class MyForm(QWidget):
    COMS=["COM8", "COM24", "COM7"]
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.ui = Ui_DTVTestForm()
        self.ui.setupUi(self)
        
        self.ui.radioButtonCOM1.setText(self.COMS[0])
        self.ui.radioButtonCOM2.setText(self.COMS[1])
        self.ui.radioButtonCOM3.setText(self.COMS[2])
        # Connection
        self.ui.pushButton_Init.clicked.connect(self.submitContact)
        self.ui.pushButton_Poweron.clicked.connect(self.submitPowerOn)
        self.ui.pushButton_Finalize.clicked.connect(self.finalizeSerial)
        self.ui.pbUnitn.clicked.connect(self.unitTestn)
        self.ui.pbUnit.clicked.connect(self.unitTest)
        #self.ui.pbSetAll.clicked.connect(self.setAll)

        self.ui.setOneSliderB.valueChanged.connect(self.setOneB)
        self.ui.setOneSliderG.valueChanged.connect(self.setOneG)
        self.ui.setOneSliderR.valueChanged.connect(self.setOneR)
        self.ui.setOneSliderS.valueChanged.connect(self.setOneS)
        self.ui.setAllSliderLEDR.valueChanged.connect(self.setAllLEDR)
        self.ui.setAllSliderLEDG.valueChanged.connect(self.setAllLEDG)
        self.ui.setAllSliderLEDB.valueChanged.connect(self.setAllLEDB)
        self.ui.setAllSliderServo.valueChanged.connect(self.setAllServo)

        self.BAUDRATE = 500000
   
        
    def resetSerial(self, port):
        print("reset port:" + port )
        try:
            self.s.finalize()
        except:
            print(port + " not initialized, Now let it start.")
        time.sleep(0.2) # For CP2102, which is not reset after immediate finalization
        if port == self.COMS[2] :
            self.s = serialTLC.serialTLC(port,5, "", "servofixCOM3.csv")
        elif port == self.COMS[0]:
            self.s = serialTLC.serialTLC(port,10, "rgbfixCOM1.csv")
        elif port == self.COMS[1]:
            self.s = serialTLC.serialTLC(port,10, "rgbfixCOM2.csv", "servofixCOM2.csv")
        self.s.initSerial(port,self.BAUDRATE)


    def submitPowerOn(self):
        if self.s.isSerialInit:
            self.s.sendPowerOn()
        else:
            print("open port first.")

    def activeCOM(self):
        if self.ui.radioButtonCOM1.isChecked():
            return self.COMS[0]
        elif self.ui.radioButtonCOM2.isChecked():
            return self.COMS[1]
        elif self.ui.radioButtonCOM3.isChecked():
            return self.COMS[2]

    def finalizeSerial(self):
        self.s.initSerial(self.activeCOM(), self.BAUDRATE)
        self.s.finalize()
        print("finalizeSerial")

    def submitContact(self):
        name= self.activeCOM()

        if name == "":
            QMessageBox.information(self, "Empty Field","Please enter a name and address.")
            return
        else:
            QMessageBox.information(self, "Success!","Open port \"%s\"!" % name)
            self.resetSerial(name)
            
    def setOneB(self):
        self.s.setOne( self.ui.spinSetPWM.value()*4, self.ui.setOneSliderB.value() )
        self.s.updateDevice()
        self.ui.labelSetB.setText( str(self.ui.setOneSliderB.value() ))
    def setOneG(self):
        self.s.setOne( self.ui.spinSetPWM.value()*4+1, self.ui.setOneSliderG.value() )
        self.s.updateDevice()
        self.ui.labelSetG.setText( str(self.ui.setOneSliderG.value() ))
    def setOneR(self):
        self.s.setOne( self.ui.spinSetPWM.value()*4+2, self.ui.setOneSliderR.value() )
        self.s.updateDevice()
        self.ui.labelSetR.setText( str(self.ui.setOneSliderR.value() ))
    def setOneS(self):
        self.s.setOne( self.ui.spinSetPWM.value()*4+3, self.ui.setOneSliderS.value() )
        self.s.updateDevice()
        self.ui.labelSetS.setText( str(self.ui.setOneSliderS.value() ))
        
    def setAllLEDR(self):
        for i in range(0,self.s.NUM_TLCS*4):
            self.s.setOne( i*4, self.ui.setAllSliderLEDR.value() )
        self.s.updateDevice()
        self.ui.labelSetAllLEDR.setText( str(self.ui.setAllSliderLEDR.value() ))
    def setAllLEDG(self):
        for i in range(0,self.s.NUM_TLCS*4):
            self.s.setOne( i*4+1, self.ui.setAllSliderLEDG.value() )
        self.s.updateDevice()
        self.ui.labelSetAllLEDG.setText( str(self.ui.setAllSliderLEDG.value() ))
    def setAllLEDB(self):
        for i in range(0,self.s.NUM_TLCS*4):
            self.s.setOne( i*4+2, self.ui.setAllSliderLEDB.value() )
        self.s.updateDevice()
        self.ui.labelSetAllLEDB.setText( str(self.ui.setAllSliderLEDB.value() ))

    def setAllServo(self):
        for i in range(0,self.s.NUM_TLCS*16):
            if i%4==3:
                self.s.setOne( i, self.ui.setAllSliderServo.value())
        self.s.updateDevice()
        self.ui.labelSetAllServo.setText( str(self.ui.setAllSliderServo.value() ))


    def setAll(self):
        self.s.setAll(self.ui.spinSetAllValue.value())

    def unitTest(self):
        self.s.fade(0.01,1.0,100)
        #self.s.unitTest(self.ui.spinUnit.value());

    def unitTestn(self):
        repeat = self.ui.spinUnitn.value()
        for i in range(0, repeat):
            self.s.unitTest(self.ui.spinUnit.value());

if __name__ == "__main__":
    app = QApplication(sys.argv)
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())