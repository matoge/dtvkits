# coding:utf-8
# 複数 seiral を扱うためのクラス
# Thread インスタンスを複数持つ。

import serialTLC,DTVTCPServer
import math
import queue, csv, time, socket,json
from queue import Queue
from threading import Thread, Lock

class serialTLCMulti:
    
    # ローカル変数はここに
    _Queues = [Queue(5),Queue(5),Queue(5)] # 各スレッドに使う Queue. サイズは一つだけ。残りは捨てる。
    _QueuesR = [Queue(),Queue(),Queue()]
    _Threads = [] # Thread は init で初期化する。

    _ThreadServer = Thread() # サーバー用スレッド
    _QueueServ = Queue(1) 
    _QueueServR = Queue() 

    _Rumtime = [0,0,0] # 電源を入れてからの時間。５分で強制的に切る
    DEADTIME = 300
    COMS = ["COM8", "COM9", "COM7"]
    RGBFIXFILES = ["rgbfixCOM1.csv", "rgbfixCOM2.csv", ""]
    SERVOFIXFILES = ["", "", "servofixCOM3.csv"]
        # それぞれのシリアルがもっている TLC5940 の数。コントローラの内部プログラムと同じでなければならない。
    NUMTLCS = [10, 10, 5]
    NUMTOTAL = sum(NUMTLCS)
    BAUDRATE = 500000
    MAXSERVOMOVE = 250

    _lock = Lock()

    _arryAll = sum(NUMTLCS) * 16* [0] 

    def __init__(self):

        # マイコン用スレッド
        self._Threads.append(Thread(target=self.runTlcThread, args=(self._Queues[0],0,)))
        self._Threads.append(Thread(target=self.runTlcThread, args=(self._Queues[1],1,)))
        self._Threads.append(Thread(target=self.runTlcThread, args=(self._Queues[2],2,)))

        for th in self._Threads:
            th.setDaemon(True)
            th.start()

        # サーバー用スレッド
        self._ThreadServer = Thread(target=self.runServerThread, args=(None,))
        self._ThreadServer.start()

    # スレッドクラスは使わず、関数のみでスレッドを実現する。
    # これをクラスのようなものだと思えばいい
    # 関数、変数はなぜか外部の者でもスレッドセーフに使えるようだ。
    def runTlcThread(self, q, index):

        qR = self._QueuesR[index]
        sport = self.COMS[index]
        # ここで初期化し、シリアルオブジェクトはこのスレッド内のみで保持する。
        s = serialTLC.serialTLC(sport,self.NUMTLCS[index], self.RGBFIXFILES[index], self.SERVOFIXFILES[index])

        # フィルタリングは、 currentData = currentData + max/min
        # (currentGoal-currentData)
        # のような感じで、ある程度動きを抑える。
        # 外部からは、currentGoal のみ変更する。
        currentData = (self.NUMTLCS[index] * 16) * [0] # 現在の位置を保持しておく
        currentGoal = (self.NUMTLCS[index] * 16) * [0]# 現在の目標位置
        print("thread started at " + sport)

        while True:

            if time.time() - self._Rumtime[index] > self.DEADTIME and s.isInit:
                print("DEADTIME. Force quit.")
                s.initSerial(sport, self.BAUDRATE)
                s.finalize()

            try:
                # 10 msec ブロックし、キューが来たら処理、来なかったら Queue.empty を送出
                tpl = q.get(True,0.01)
                if len(tpl) < 1:
                    print(sport + ":command invalid. ignored.")
                    continue

                command = tpl[0]
                #print(sport + ":command is" + command)
                
                if command == "COM_INIT":
                    s.initSerial(sport , self.BAUDRATE)
                    qR.put("DONEINIT")
                    
                if command == "COM_POWERON":
                    if s.isSerialInit == True:
                        s.sendPowerOn()
                        self._Rumtime[index] = time.time()
                if command == "COM_POWEROFF":
                    if s.isSerialInit == True:
                        s.initSerial(sport, self.BAUDRATE)
                        s.finalize()

                elif command == "COM_CLOSE":
                    if s.isSerialInit:
                        s.finalize()
                    else:
                        print( sport + ": not opened.")
                elif command == "COM_SET": # currentGoal の設定し、すぐにスムージングして送出
                    print("I am " + sport + ". got " + str(tpl[0]))
                    if s.isInit: # パワーがはいっている状態
                        if len(tpl) == 2:
                            arr = tpl[1]
                            #(arr)
                            if len(arr) == self.NUMTLCS[index] * 16:
                                currentGoal = tpl[1]
                                #currentData = currentGoal
                                out = self.smoothArray(arr, currentData)
                                s.setAllData(out)
                            else:
                                assert False, sport+":data size is not correct. got " + str(len(arr))
                        else:
                            assert False, sport+":command size is not correct for COM_SET"
                        #print(sport+":not powered")

                q.task_done()

            except queue.Empty: # キューが来なかったら、スムージングのみの処理を行う。
                if s.isInit == True and s.isSerialInit == True:
                    if currentGoal != currentData :
                        currentData = self.smoothArray(currentGoal, currentData)
                        s.setAllData(currentData)
                        print(sport+ ":smoothed")
                else:  
                    print(sport+":not init. Wait 1 second.")
                    time.sleep(1)

    def runServerThread(self, qServe=Queue):
        """
        SocketServer は毎回スレッドを生成して効率が悪いので、シンプルなソケットサーバーで listen する。
        """
        #server = DTVTCPServer.DTVTCPServer(('127.0.0.1', 13374), DTVTCPServer.DTVTCPServerHandler)
        
        host = socket.gethostbyname('localhost')
        port = 13374

        while True:
            t = time.time()
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(0.1)
            sock.bind((host, port))
            sock.listen(5)
            #print("listening")
            try:
                (client_sock, client_addr) = sock.accept()
                #print(client_addr)
                msg = client_sock.recv(1024*4)
                #print("received")
                if msg != b'':
                    getData= json.loads((msg.strip()).decode('utf-8'))
                    print("command:" + getData['DTV_COMMAND'] + ", Data Length:" + str(len(getData['DTV_DATA'])))
                    self._arryAll = getData['DTV_DATA']
                    self.updateAllPorts()
                        #que.put((data,))
                    # send some 'ok' back
                    client_sock.send(bytes(json.dumps({'return':'ok'}),'UTF-8'))
                    
                client_sock.close()
                sock.close()
            except:
                print("timeout?")
            #print(time.time()-t)

        #while True:
        #    try:
        #        server.handle_request()
        #        print("tcpreceived:" + server.getData['DTV_COMMAND'])
        #         ここに
        #        self._arryAll = server.getData['DTV_DATA']
        #        self.updateAllPorts()
        #    except Exception as e:
        #        print("caught exception. Just ignore. ", e)
                
                
    def smoothArray(self, goalArr, currArr):
        # LED はそのままでよい。サーボのみ suppress する。
        for i in range( int(len(currArr) / 4)):
            currArr[i * 4] = goalArr[i * 4]
            currArr[i * 4 + 1] = goalArr[i * 4 + 1]
            currArr[i * 4 + 2] = goalArr[i * 4 + 2]

            idx = i * 4 + 3 # サーボの位置
            dif = goalArr[idx] - currArr[idx]
            #currArr[idx] = goalArr[idx]
            #print("smoothed:" + str(dif))
            if abs(dif) < self.MAXSERVOMOVE:
                currArr[idx] = goalArr[idx]
            else:
                currArr[idx] = currArr[idx] + math.copysign(1,dif) *self.MAXSERVOMOVE
            #rint(math.copysign(0, dif) * min([self.MAXSERVOMOVE, abs(dif)]))
        return currArr
    
    def initOne(self, idx):
        self._Queues[idx].put(("COM_INIT",))
    def powerOne(self, idx):
        self._Queues[idx].put(("COM_POWERON",))
    def powerOffOne(self, idx):
        self._Queues[idx].put(("COM_POWEROFF",))
    def closeOne(self, idx):
        self._Queues[idx].put(("COM_CLOSE",))

    # アップデートはしない
    # serialTLCMulti は、個々のサーボの調整は行わない。
    # serialTLC.SetAllData() にてサーボのデッドバンドに基づいた対応をする。
    def setOne(self, channel, value):
        self._arryAll[channel] = value

    # アップデートはしない
    def setAll(self, value):
        self._arryAll = sum(self.NUMTLCS) * 16* [value] 

    # すべてのポートに送出
    def updateAllPorts(self):
        """ 
        １００個をアップデート
        """
        # 複数からのアクセスをロックする. ロックしなくてもqueueにputするだけなので問題は起こらない。
        try:
            self._Queues[0].put(("COM_SET", self._arryAll[0:160]),False)
        except queue.Full:
            pass
            #一個捨てて無理やり入れる
            #self._Queues[0].get()
            #self._Queues[0].put(("COM_SET", self._arryAll[0:160]),False)
            #print("too fast. order abandoned.")
        try:
            self._Queues[1].put(("COM_SET", self._arryAll[160:320]),False)
        except queue.Full:
            #self._Queues[1].get()
            #self._Queues[1].put(("COM_SET", self._arryAll[160:320]),False)
            #print("too fast. order abandoned.")
            pass
        try:
            self._Queues[2].put(("COM_SET", self._arryAll[320:400]),False)
        except queue.Full:
            #self._Queues[2].get()
            #self._Queues[2].put(("COM_SET", self._arryAll[320:400]),False)
            #print("too fast. order abandoned.")
            pass


    # テスト用パターンスレッドで送出し、いつでもすぐに止められるようにする
    def fade():
        pass

    def join(self):
        for q in self._Queues:
            q.join()

    def doOriginalPosition(self):
        oldth = self.MAXSERVOMOVE
        self.MAXSERVOMOVE = 35
        self.setAll(1)
        self.updateAllPorts()
        time.sleep(3)
        self.setAll(0) # 次の実行のため
        self.MAXSERVOMOVE = oldth

    def safePowerOff(self):
        self.doOriginalPosition()
        self.powerOffOne(0)
        self.powerOffOne(1)
        self.powerOffOne(2)

if __name__ == '__main__':
    tlcmulti = serialTLCMulti()
    tlcmulti.initOne(0)
    print(tlcmulti._QueuesR[0].get()) # 一応シェイクハンド的なことをしておく
    tlcmulti.powerOne(0)
    tlcmulti.powerOffOne(0)
    tlcmulti.join()
    print("main thread finished")
