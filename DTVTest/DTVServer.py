# coding:utf-8

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import sys, time
from multiUI import Ui_DTVServer
import  serialTLCMulti
 
class DTVServer(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.ui = Ui_DTVServer()
        self.ui.setupUi(self)
        
        self.ui.pbInit1.clicked.connect(self.init1)
        self.ui.pbInit2.clicked.connect(self.init2)
        self.ui.pbInit3.clicked.connect(self.init3)
        self.ui.pbInitAll.clicked.connect(self.initAll)
        self.ui.pbPowerOff.clicked.connect(self.powerOff)
        self.ui.setOneSliderR.valueChanged.connect(self.setOneLEDR)
        self.ui.setOneSliderG.valueChanged.connect(self.setOneLEDG)
        self.ui.setOneSliderB.valueChanged.connect(self.setOneLEDB)
        self.ui.setOneSliderS.valueChanged.connect(self.setOneServo)
        self.ui.setAllSliderLEDR.valueChanged.connect(self.setAllLEDR)
        self.ui.setAllSliderLEDG.valueChanged.connect(self.setAllLEDG)
        self.ui.setAllSliderLEDB.valueChanged.connect(self.setAllLEDB)
        self.ui.setAllSliderServo.valueChanged.connect(self.setAllServo)
        # ui and functions

        # variables
        self.sm = serialTLCMulti.serialTLCMulti()
        
    def init1(self,idx):
        self.sm.initOne(0)
        print(self.sm._QueuesR[0].get()) # Shake hand
        self.sm.powerOne(0)
    def init2(self,idx):
        self.sm.initOne(1)
        print(self.sm._QueuesR[1].get()) # Shake hand
        self.sm.powerOne(1)
    def init3(self,idx):
        self.sm.initOne(2)
        print(self.sm._QueuesR[2].get()) # Shake hand
        self.sm.powerOne(2)

    def powerOff(self):
        self.sm.safePowerOff()

    def initAll(self):
        self.sm.initOne(0)
        self.sm.initOne(1)
        self.sm.initOne(2)
        print(self.sm._QueuesR[0].get()) # Shake hand
        print(self.sm._QueuesR[1].get()) # Shake hand
        print(self.sm._QueuesR[2].get()) # Shake hand
        self.sm.powerOne(0)
        self.sm.powerOne(1)
        self.sm.powerOne(2)
        
    def setOneLEDR(self):
        module = self.ui.spinOne.value()
        value=self.ui.setOneSliderR.value() *64 
        self.sm.setOne( module*4 , value)
        self.sm.updateAllPorts()
        self.ui.labelSetR.setText( str(value))
    def setOneLEDG(self):
        module = self.ui.spinOne.value()
        value=self.ui.setOneSliderG.value() *64 
        self.sm.setOne( module*4+1 , value)
        self.sm.updateAllPorts()
        self.ui.labelSetG.setText( str(value))
    def setOneLEDB(self):
        module = self.ui.spinOne.value()
        value=self.ui.setOneSliderB.value() *64 
        self.sm.setOne( module*4+2 , value)
        self.sm.updateAllPorts()
        self.ui.labelSetB.setText( str(value))
    def setOneServo(self):
        module = self.ui.spinOne.value()
        value=self.ui.setOneSliderS.value() *64 
        self.sm.setOne( module*4+3 , value)
        self.sm.updateAllPorts()
        self.ui.labelSetS.setText( str(value))

    def setAllLEDR(self):
        for i in range(self.sm.NUMTOTAL*4):
            self.sm.setOne( i*4,  self.ui.setAllSliderLEDR.value() *64 )
        self.sm.updateAllPorts()
        self.ui.labelSetAllLEDR.setText( str(self.ui.setAllSliderLEDR.value() *64 ))
    def setAllLEDG(self):
        for i in range(self.sm.NUMTOTAL*4):
            self.sm.setOne( i*4+1, self.ui.setAllSliderLEDG.value() *64 )
        self.sm.updateAllPorts()
        self.ui.labelSetAllLEDG.setText( str(self.ui.setAllSliderLEDG.value() *64 ))
    def setAllLEDB(self):
        for i in range(self.sm.NUMTOTAL*4):
            self.sm.setOne( i*4+2,  self.ui.setAllSliderLEDB.value() *64 )
        self.sm.updateAllPorts()
        self.ui.labelSetAllLEDB.setText( str(self.ui.setAllSliderLEDB.value() *64 ))
    def setAllServo(self):
        for i in range(self.sm.NUMTOTAL*4):
            self.sm.setOne( i*4+3,  self.ui.setAllSliderServo.value() )
        self.sm.updateAllPorts()
        self.ui.labelSetAllServo.setText( str(self.ui.setAllSliderServo.value() ))



if __name__ == "__main__":
    app = QApplication(sys.argv)
    myapp = DTVServer()
    myapp.show()
    sys.exit(app.exec_())  