# coding:utf-8

import socketserver, socket
import json
from queue import Queue
from threading import Thread

class DTVTCPServer(socketserver.ThreadingTCPServer):
    def __init__(self, server_address, RequestHandlerClass):
        #self.timeout = 1
        self.getData = None
        allow_reuse_address = True
        socketserver.TCPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate=True)
    def handle_timeout(self):
        print("timedout")

class DTVTCPServerHandler(socketserver.BaseRequestHandler):
    """

    """
    def handle(self):
        #que = self.server.dataQue # データを乗っけるキュー
        try:
            self.server.getData= json.loads((self.request.recv(1024*4).strip()).decode('utf-8'))
            #que.put((data,))
            # send some 'ok' back
            self.request.sendall(bytes(json.dumps({'return':'ok'}),'UTF-8'))
        except Exception as e:
            print("Exception while receiving message: ", e)

# もっと単純なサーバ

def testGetQue(que=Queue):
    while True:
        print("get;"+str(que.get()))

if __name__ == '__main__':
    #server = DTVTCPServer(('127.0.0.1', 13374), DTVTCPServerHandler)
    #thread = Thread(target=testGetQue)
    #thread.start();
    host = socket.gethostbyname('localhost')
    port = 13374

    while True:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind((host, port))
        sock.listen(1)
        print("listening")
        (client_sock, client_addr) = sock.accept()
        print(client_addr)
        msg = client_sock.recv(1024*4)
        #print(msg)
        if msg != b'':
            getData= json.loads((msg.strip()).decode('utf-8'))
                #que.put((data,))
            # send some 'ok' back
            client_sock.send(bytes(json.dumps({'return':'ok'}),'UTF-8'))
        client_sock.close()
        sock.close()
    #while True:
    #    try:
    #        print("handling request")
    #        server.handle_request()
    #        print("command:" + server.getData['DTV_COMMAND'] + ", Data Length:" + str(len( server.getData['DTV_DATA'])))

    #    except Exception as e:
    #        print("caught exception. Just ignore. ", e)
                


