# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'multiUI.ui'
#
# Created: Tue Jun 24 20:09:10 2014
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DTVServer(object):
    def setupUi(self, DTVServer):
        DTVServer.setObjectName("DTVServer")
        DTVServer.resize(572, 586)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(DTVServer.sizePolicy().hasHeightForWidth())
        DTVServer.setSizePolicy(sizePolicy)
        self.gridLayout_4 = QtWidgets.QGridLayout(DTVServer)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.gbMultiDevice = QtWidgets.QGroupBox(DTVServer)
        self.gbMultiDevice.setObjectName("gbMultiDevice")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.gbMultiDevice)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.lSerial1 = QtWidgets.QLabel(self.gbMultiDevice)
        self.lSerial1.setObjectName("lSerial1")
        self.gridLayout_3.addWidget(self.lSerial1, 0, 0, 1, 1)
        self.spinSerial1 = QtWidgets.QSpinBox(self.gbMultiDevice)
        self.spinSerial1.setProperty("value", 24)
        self.spinSerial1.setObjectName("spinSerial1")
        self.gridLayout_3.addWidget(self.spinSerial1, 0, 1, 1, 1)
        self.pbInit1 = QtWidgets.QPushButton(self.gbMultiDevice)
        self.pbInit1.setObjectName("pbInit1")
        self.gridLayout_3.addWidget(self.pbInit1, 0, 2, 1, 1)
        self.lSerial2 = QtWidgets.QLabel(self.gbMultiDevice)
        self.lSerial2.setObjectName("lSerial2")
        self.gridLayout_3.addWidget(self.lSerial2, 1, 0, 1, 1)
        self.spinSerial2 = QtWidgets.QSpinBox(self.gbMultiDevice)
        self.spinSerial2.setProperty("value", 24)
        self.spinSerial2.setObjectName("spinSerial2")
        self.gridLayout_3.addWidget(self.spinSerial2, 1, 1, 1, 1)
        self.pbInit2 = QtWidgets.QPushButton(self.gbMultiDevice)
        self.pbInit2.setObjectName("pbInit2")
        self.gridLayout_3.addWidget(self.pbInit2, 1, 2, 1, 1)
        self.lSerial3 = QtWidgets.QLabel(self.gbMultiDevice)
        self.lSerial3.setObjectName("lSerial3")
        self.gridLayout_3.addWidget(self.lSerial3, 2, 0, 1, 1)
        self.spinSerial3 = QtWidgets.QSpinBox(self.gbMultiDevice)
        self.spinSerial3.setProperty("value", 24)
        self.spinSerial3.setObjectName("spinSerial3")
        self.gridLayout_3.addWidget(self.spinSerial3, 2, 1, 1, 1)
        self.pbInit3 = QtWidgets.QPushButton(self.gbMultiDevice)
        self.pbInit3.setObjectName("pbInit3")
        self.gridLayout_3.addWidget(self.pbInit3, 2, 2, 1, 1)
        self.pbInitAll = QtWidgets.QPushButton(self.gbMultiDevice)
        self.pbInitAll.setObjectName("pbInitAll")
        self.gridLayout_3.addWidget(self.pbInitAll, 3, 0, 1, 3)
        self.pbPowerOff = QtWidgets.QPushButton(self.gbMultiDevice)
        self.pbPowerOff.setObjectName("pbPowerOff")
        self.gridLayout_3.addWidget(self.pbPowerOff, 4, 0, 1, 3)
        self.gridLayout_4.addWidget(self.gbMultiDevice, 0, 0, 1, 1)
        self.groupBox = QtWidgets.QGroupBox(DTVServer)
        self.groupBox.setObjectName("groupBox")
        self.formLayout_5 = QtWidgets.QFormLayout(self.groupBox)
        self.formLayout_5.setObjectName("formLayout_5")
        self.pbUnit = QtWidgets.QPushButton(self.groupBox)
        self.pbUnit.setObjectName("pbUnit")
        self.formLayout_5.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.pbUnit)
        self.spinUnit = QtWidgets.QSpinBox(self.groupBox)
        self.spinUnit.setObjectName("spinUnit")
        self.formLayout_5.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.spinUnit)
        self.pbUnitn = QtWidgets.QPushButton(self.groupBox)
        self.pbUnitn.setObjectName("pbUnitn")
        self.formLayout_5.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.pbUnitn)
        self.spinUnitn = QtWidgets.QSpinBox(self.groupBox)
        self.spinUnitn.setObjectName("spinUnitn")
        self.formLayout_5.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.spinUnitn)
        self.gridLayout_4.addWidget(self.groupBox, 0, 1, 1, 1)
        self.groupBox_6 = QtWidgets.QGroupBox(DTVServer)
        self.groupBox_6.setObjectName("groupBox_6")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.groupBox_6)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.labelSetB = QtWidgets.QLabel(self.groupBox_6)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSetB.sizePolicy().hasHeightForWidth())
        self.labelSetB.setSizePolicy(sizePolicy)
        self.labelSetB.setMinimumSize(QtCore.QSize(40, 0))
        self.labelSetB.setAlignment(QtCore.Qt.AlignCenter)
        self.labelSetB.setObjectName("labelSetB")
        self.gridLayout_2.addWidget(self.labelSetB, 5, 2, 1, 1)
        self.labelSet1 = QtWidgets.QLabel(self.groupBox_6)
        self.labelSet1.setObjectName("labelSet1")
        self.gridLayout_2.addWidget(self.labelSet1, 0, 0, 1, 1)
        self.setOneSliderS = QtWidgets.QSlider(self.groupBox_6)
        self.setOneSliderS.setMaximum(63)
        self.setOneSliderS.setPageStep(1)
        self.setOneSliderS.setOrientation(QtCore.Qt.Horizontal)
        self.setOneSliderS.setObjectName("setOneSliderS")
        self.gridLayout_2.addWidget(self.setOneSliderS, 6, 0, 1, 2)
        self.labelSetS = QtWidgets.QLabel(self.groupBox_6)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSetS.sizePolicy().hasHeightForWidth())
        self.labelSetS.setSizePolicy(sizePolicy)
        self.labelSetS.setMinimumSize(QtCore.QSize(40, 0))
        self.labelSetS.setAlignment(QtCore.Qt.AlignCenter)
        self.labelSetS.setObjectName("labelSetS")
        self.gridLayout_2.addWidget(self.labelSetS, 6, 2, 1, 1)
        self.setOneSliderB = QtWidgets.QSlider(self.groupBox_6)
        self.setOneSliderB.setMaximum(63)
        self.setOneSliderB.setSingleStep(1)
        self.setOneSliderB.setPageStep(1)
        self.setOneSliderB.setOrientation(QtCore.Qt.Horizontal)
        self.setOneSliderB.setObjectName("setOneSliderB")
        self.gridLayout_2.addWidget(self.setOneSliderB, 5, 0, 1, 2)
        self.spinOne = QtWidgets.QSpinBox(self.groupBox_6)
        self.spinOne.setMaximum(99)
        self.spinOne.setObjectName("spinOne")
        self.gridLayout_2.addWidget(self.spinOne, 0, 1, 1, 1)
        self.labelSetG = QtWidgets.QLabel(self.groupBox_6)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSetG.sizePolicy().hasHeightForWidth())
        self.labelSetG.setSizePolicy(sizePolicy)
        self.labelSetG.setMinimumSize(QtCore.QSize(40, 0))
        self.labelSetG.setAlignment(QtCore.Qt.AlignCenter)
        self.labelSetG.setObjectName("labelSetG")
        self.gridLayout_2.addWidget(self.labelSetG, 4, 2, 1, 1)
        self.labelSetR = QtWidgets.QLabel(self.groupBox_6)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSetR.sizePolicy().hasHeightForWidth())
        self.labelSetR.setSizePolicy(sizePolicy)
        self.labelSetR.setMinimumSize(QtCore.QSize(40, 0))
        self.labelSetR.setAlignment(QtCore.Qt.AlignCenter)
        self.labelSetR.setObjectName("labelSetR")
        self.gridLayout_2.addWidget(self.labelSetR, 3, 2, 1, 1)
        self.setOneSliderR = QtWidgets.QSlider(self.groupBox_6)
        self.setOneSliderR.setMaximum(63)
        self.setOneSliderR.setSingleStep(1)
        self.setOneSliderR.setPageStep(1)
        self.setOneSliderR.setOrientation(QtCore.Qt.Horizontal)
        self.setOneSliderR.setObjectName("setOneSliderR")
        self.gridLayout_2.addWidget(self.setOneSliderR, 3, 0, 1, 2)
        self.setOneSliderG = QtWidgets.QSlider(self.groupBox_6)
        self.setOneSliderG.setMaximum(63)
        self.setOneSliderG.setSingleStep(1)
        self.setOneSliderG.setPageStep(1)
        self.setOneSliderG.setOrientation(QtCore.Qt.Horizontal)
        self.setOneSliderG.setObjectName("setOneSliderG")
        self.gridLayout_2.addWidget(self.setOneSliderG, 4, 0, 1, 2)
        self.gridLayout_4.addWidget(self.groupBox_6, 2, 0, 1, 2)
        self.groupBox_7 = QtWidgets.QGroupBox(DTVServer)
        self.groupBox_7.setObjectName("groupBox_7")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox_7)
        self.gridLayout.setObjectName("gridLayout")
        self.labelSetAllLEDG = QtWidgets.QLabel(self.groupBox_7)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSetAllLEDG.sizePolicy().hasHeightForWidth())
        self.labelSetAllLEDG.setSizePolicy(sizePolicy)
        self.labelSetAllLEDG.setMinimumSize(QtCore.QSize(40, 0))
        self.labelSetAllLEDG.setAlignment(QtCore.Qt.AlignCenter)
        self.labelSetAllLEDG.setObjectName("labelSetAllLEDG")
        self.gridLayout.addWidget(self.labelSetAllLEDG, 2, 2, 1, 1)
        self.labelSetAllLEDR = QtWidgets.QLabel(self.groupBox_7)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSetAllLEDR.sizePolicy().hasHeightForWidth())
        self.labelSetAllLEDR.setSizePolicy(sizePolicy)
        self.labelSetAllLEDR.setMinimumSize(QtCore.QSize(40, 0))
        self.labelSetAllLEDR.setAlignment(QtCore.Qt.AlignCenter)
        self.labelSetAllLEDR.setObjectName("labelSetAllLEDR")
        self.gridLayout.addWidget(self.labelSetAllLEDR, 1, 2, 1, 1)
        self.setAllSliderLEDG = QtWidgets.QSlider(self.groupBox_7)
        self.setAllSliderLEDG.setMaximum(63)
        self.setAllSliderLEDG.setPageStep(1)
        self.setAllSliderLEDG.setOrientation(QtCore.Qt.Horizontal)
        self.setAllSliderLEDG.setObjectName("setAllSliderLEDG")
        self.gridLayout.addWidget(self.setAllSliderLEDG, 2, 0, 1, 2)
        self.setAllSliderLEDB = QtWidgets.QSlider(self.groupBox_7)
        self.setAllSliderLEDB.setMaximum(63)
        self.setAllSliderLEDB.setPageStep(1)
        self.setAllSliderLEDB.setOrientation(QtCore.Qt.Horizontal)
        self.setAllSliderLEDB.setObjectName("setAllSliderLEDB")
        self.gridLayout.addWidget(self.setAllSliderLEDB, 3, 0, 1, 2)
        self.labelSet2_2 = QtWidgets.QLabel(self.groupBox_7)
        self.labelSet2_2.setObjectName("labelSet2_2")
        self.gridLayout.addWidget(self.labelSet2_2, 0, 0, 1, 1)
        self.labelSetAllLEDB = QtWidgets.QLabel(self.groupBox_7)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSetAllLEDB.sizePolicy().hasHeightForWidth())
        self.labelSetAllLEDB.setSizePolicy(sizePolicy)
        self.labelSetAllLEDB.setMinimumSize(QtCore.QSize(40, 0))
        self.labelSetAllLEDB.setAlignment(QtCore.Qt.AlignCenter)
        self.labelSetAllLEDB.setObjectName("labelSetAllLEDB")
        self.gridLayout.addWidget(self.labelSetAllLEDB, 3, 2, 1, 1)
        self.labelSet2_3 = QtWidgets.QLabel(self.groupBox_7)
        self.labelSet2_3.setObjectName("labelSet2_3")
        self.gridLayout.addWidget(self.labelSet2_3, 4, 0, 1, 1)
        self.labelSetAllServo = QtWidgets.QLabel(self.groupBox_7)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelSetAllServo.sizePolicy().hasHeightForWidth())
        self.labelSetAllServo.setSizePolicy(sizePolicy)
        self.labelSetAllServo.setMinimumSize(QtCore.QSize(40, 0))
        self.labelSetAllServo.setAlignment(QtCore.Qt.AlignCenter)
        self.labelSetAllServo.setObjectName("labelSetAllServo")
        self.gridLayout.addWidget(self.labelSetAllServo, 5, 2, 1, 1)
        self.setAllSliderServo = QtWidgets.QSlider(self.groupBox_7)
        self.setAllSliderServo.setMaximum(4095)
        self.setAllSliderServo.setOrientation(QtCore.Qt.Horizontal)
        self.setAllSliderServo.setObjectName("setAllSliderServo")
        self.gridLayout.addWidget(self.setAllSliderServo, 5, 0, 1, 2)
        self.setAllSliderLEDR = QtWidgets.QSlider(self.groupBox_7)
        self.setAllSliderLEDR.setMaximum(63)
        self.setAllSliderLEDR.setSingleStep(1)
        self.setAllSliderLEDR.setPageStep(1)
        self.setAllSliderLEDR.setSliderPosition(0)
        self.setAllSliderLEDR.setOrientation(QtCore.Qt.Horizontal)
        self.setAllSliderLEDR.setInvertedControls(False)
        self.setAllSliderLEDR.setTickPosition(QtWidgets.QSlider.TicksAbove)
        self.setAllSliderLEDR.setTickInterval(1)
        self.setAllSliderLEDR.setObjectName("setAllSliderLEDR")
        self.gridLayout.addWidget(self.setAllSliderLEDR, 1, 0, 1, 2)
        self.gridLayout_4.addWidget(self.groupBox_7, 3, 0, 1, 2)

        self.retranslateUi(DTVServer)
        QtCore.QMetaObject.connectSlotsByName(DTVServer)

    def retranslateUi(self, DTVServer):
        _translate = QtCore.QCoreApplication.translate
        DTVServer.setWindowTitle(_translate("DTVServer", "Form"))
        self.gbMultiDevice.setTitle(_translate("DTVServer", "Multiple Device"))
        self.lSerial1.setText(_translate("DTVServer", "Serial1:COM"))
        self.pbInit1.setText(_translate("DTVServer", "Init1"))
        self.lSerial2.setText(_translate("DTVServer", "Serial2:COM"))
        self.pbInit2.setText(_translate("DTVServer", "Init2"))
        self.lSerial3.setText(_translate("DTVServer", "Serial3:COM"))
        self.pbInit3.setText(_translate("DTVServer", "Init3"))
        self.pbInitAll.setText(_translate("DTVServer", "InitAll"))
        self.pbPowerOff.setText(_translate("DTVServer", "Power Off"))
        self.groupBox.setTitle(_translate("DTVServer", "Unit Test"))
        self.pbUnit.setText(_translate("DTVServer", "UnitTest"))
        self.pbUnitn.setText(_translate("DTVServer", "U.T. n Times"))
        self.groupBox_6.setTitle(_translate("DTVServer", "Set One"))
        self.labelSetB.setText(_translate("DTVServer", "0"))
        self.labelSet1.setText(_translate("DTVServer", "Module"))
        self.labelSetS.setText(_translate("DTVServer", "0"))
        self.labelSetG.setText(_translate("DTVServer", "0"))
        self.labelSetR.setText(_translate("DTVServer", "0"))
        self.groupBox_7.setTitle(_translate("DTVServer", "Set All"))
        self.labelSetAllLEDG.setText(_translate("DTVServer", "0"))
        self.labelSetAllLEDR.setText(_translate("DTVServer", "0"))
        self.labelSet2_2.setText(_translate("DTVServer", "LED"))
        self.labelSetAllLEDB.setText(_translate("DTVServer", "0"))
        self.labelSet2_3.setText(_translate("DTVServer", "Servo"))
        self.labelSetAllServo.setText(_translate("DTVServer", "0"))

