# coding:utf-8

# 一つの Arduino と通信するための ライブラリ。
# serial が必要
# NUM_TLCS は処理によるレイテンシを

import os, copy, csv
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

import serial, array, math, time
from struct import *


class serialTLC:
    def __init__(self, com, numtlcs,rgbfix="", servofix=""):
        self.NUM_TLCS=numtlcs
        self.ser = None # シリアルポート
        self.arr = array.array('H') #unsigned short
        self.arr.fromlist([0]*self.NUM_TLCS*16) # 必要な数だけ。
        self.FADESPEEDLIMIT = 8
        self.SAFETYDELAY = 0.02
        self.GAMMACOEF = 2
        self.MIN_WIDTH=100 # デフォルトのサーボ
        self.MAX_WIDTH=480 # 500 in the case of Z
        self.isSerialInit = False
        self.isInit = False
        self.alignment = self.readAlignFile("alignment.csv")
        self.com = com

        if( rgbfix != ""):
            self.rgbfix = self.readRGBFix(rgbfix)
        else:
            self.rgbfix = []

        self.sfixMin = (self.NUM_TLCS*4)*[self.MIN_WIDTH] # index は実際のモジュール番号になる、注意
        self.sfixMax = (self.NUM_TLCS*4)*[self.MAX_WIDTH]
        self.readServoFix(servofix)

        print(self.rgbfix)

    def readAlignFile(self, filename):
        arrout = [[0 for j in range(4)] for i in range(10)]
        idx=0
                                                       
        for row in csv.reader(open(filename)):
            for i in range(len(row)):
                row[i] = int (row[i])
            arrout[idx] = row
            idx=idx+1
        print("Alignment read:" + str(arrout))
        return arrout
    
    def readRGBFix(self, filename):
        arrout = []
        for row in csv.reader(open(filename)):
            arrout.append((int(row[0]), int(row[1]),row[2])) # x,y,
        return arrout

    def readServoFix(self, filename):
        # Servo は x, y, MIN, MAX の順で入れる
        arrout = []
        fixrows = []
        if filename != "":
            fixrows = csv.reader(open(filename))
        for row in fixrows:
            # すべて updatedevice で実際のデバイスに割り当てられるので、
            # 見かけの座標をそのまま入れる
            self.sfixMin[ int(row[0])+10*int(row[1]) ] = int(row[2])
            self.sfixMax[ int(row[0])+10*int(row[1]) ] = int(row[3])
        print(self.com +":Servofix read:" + str(self.sfixMin) + "\n" + str(self.sfixMax))

    def initSerial(self,com,baud=500000):
        # Arduino is to be reset 
        self.com = com
        try:
            self.ser = serial.Serial(com, baud, timeout=1)
        except:
            self.ser.close()
            time.sleep(self.SAFETYDELAY) 
            self.ser = serial.Serial(com, baud, timeout=1)
            return

        self.isSerialInit = True
        print("port:" + com +" initialized")

        # 電源を入れるシェイクハンド


    # 強制的に電源を切る
    def sendReset():
        print("Force power off")
        self.ser.flush() # Flush any unwanted data
        self.ser.write(b':reset;') 
        checkState = self.ser.readline().strip() 
        assert checkState == b'RESETDONE', "Something wrong on device. " + str(checkState) + " returned."
        print("Reset Success. Now MCT is waiting for POWERON signal.")

    #def sendCommand(command):
    #    self.ser.write(command) 
    #    checkState = self.ser.readline().strip() 
    #    assert checkState == command + b'ok', "Something wrong on device. " + str(checkState) + " returned."

    # 電源を入れるシーケンス。電源は慎重に入れる
    def sendPowerOn(self):
        rel=""
        to = 0 
        while rel != b'SERIALOK' and to < 15:
            rel = self.ser.readline().strip() 
            print("Waiting for SERIALOK returned..." + str(rel))

        if to == 15:
            assert False, "initok does not returned. Wrong with MCU."
            
        self.ser.write(b':init;') 
        rel = self.ser.readline().strip() 
        assert rel==b'initok', "initok does not returned. Wrong with MCU."
        # finalize initialization
        self.ser.write(b':')

        rel = self.ser.readline().strip() 
        assert rel == b'WAITPOWERON', "Not WAITPOWERON returned. " + str(rel) + " returned."
        
        rel=""
        to=0
        print("Waiting...")
        # If failure, port is closed. 
        while rel != b'POWERON' and to<10:
            rel = self.ser.readline().strip()
            print(self.com+":Waiting power..")
            to=to+1
        if to == 10:
            print(self.com+":"+"Power is not ready.")
            self.finalize()
            return False
        else:
            print("POWERON returned. Now DTV device on "+ self.com+ " ready")
            self.isInit = True
            return True

    def setAll(self, val):
        time.sleep(self.SAFETYDELAY) # この後時間を置かないとエラーになる。

        for j in range(self.NUM_TLCS*16):
            self.setOne(j, val)
        self.updateDevice()

    def setAllData(self, arr):
        # サーボのデッドバンドの調整はここで、
        # このアレイは、サーボの値を 0　から 4095 で取る。
        # serialTLCMulti は、個々のサーボの調整は行わない。
        for channel, val in enumerate(arr):
            self.setOne(channel, val)
        self.updateDevice()

    # loose servo All
    def looseAll(self):
        for channel in range(self.NUM_TLCS*16):
            if channel % 4 == 3:
                self.arr[channel] = 0
        self.updateDevice()
        return True
    # サーボの値をチェックして、おかしければ変える。
#    def normAll(self, val):

    def finalize(self):
        self.isInit = False
        self.ser.close()
        self.isSerialInit = False

    def setOne(self, channel, val):
        # Set value to one pwm slot.
        # Not updated
        val = max(0, min(val,4095))

        if channel % 4 == 3:
            if int(val) == 0:
                self.arr[channel] = 0
            else:
                self.arr[channel] = self.tlc_angleToVal(channel,int(val))
        else:
            self.arr[channel] = int(val)
        #self.ser.write(self._convertTo24(self.arr))
        # print("return",self.ser.readline()) # Wait for reply
        

    def updateDevice(self):
        # モジュール順番を入れ替える
        sendarr=array.array('H') #unsigned short
        sendarr.fromlist([0]*self.NUM_TLCS*16)

        for i in range(self.NUM_TLCS*4):
            xidx = i%10
            yidx = math.floor(i/10)
            idx = self.alignment[xidx][yidx]
            # print( i, idx )
            # 1番目のLEDはRGB, 2,3,4 番はGBR
            if idx%4 ==0:
                sendarr[idx*4] = self.gammaCorr(self.arr[i*4])
                sendarr[idx*4+1] = self.gammaCorr(self.arr[i*4+1])
                sendarr[idx*4+2] = self.gammaCorr(self.arr[i*4+2])
            else:
                sendarr[idx*4] = self.gammaCorr(self.arr[i*4+2])
                sendarr[idx*4+1] = self.gammaCorr(self.arr[i*4+1])
                sendarr[idx*4+2] = self.gammaCorr(self.arr[i*4])

            sendarr[idx*4+3] = self.arr[i*4+3] 


        # rgbfix
        for fix in self.rgbfix:
            fixidx = self.alignment[fix[0]][fix[1]]
            type = fix[2]
            if type == 'bgr':
                #print("fix bgr x:" + str(fix[0]) + ",y:" + str(fix[1]) + ",type:" + type)
                tmp = sendarr[fixidx*4]
                sendarr[fixidx*4] = sendarr[fixidx*4+2]
                sendarr[fixidx*4+2] = tmp
            elif type =='rbg':
                #print("fix rgb x:" + str(fix[0]) + ",y:" + str(fix[1]) + ",type:" + type)
                tmp = sendarr[fixidx*4]
                sendarr[fixidx*4] = sendarr[fixidx*4+1]
                sendarr[fixidx*4+1] = tmp
            elif type =='gbr':
                #print("fix rgb x:" + str(fix[0]) + ",y:" + str(fix[1]) + ",type:" + type)
                tmp = sendarr[fixidx*4+1]
                sendarr[fixidx*4+1] = sendarr[fixidx*4+2]
                sendarr[fixidx*4+2] = tmp

        # 
        # サーボは、見通しをよくするために、angleToVal で一括変換する。

 
        # when update, first ask the arduino "sendok" signal by sending ";send:" command
        self.ser.flush() # Flush any unwanted data
        self.ser.write(b':send;') 
        # 待つ
        checkState = self.ser.readline().strip() 
        assert checkState == b'sendok', "Something wrong on device. " + str(checkState) + " returned."

        self.ser.write(self._convertTo24(sendarr))
        #print(self._convertTo24(self.arr))
        resl = self.ser.readline().strip() # Wait for reply. should read f
        if resl == b'f' :
            #print("Device update successful. 'f' returned")
            return True
        else:
            print("Device not updated. "+ str(resl)+ " returned.")
            return False

    def gammaCorr(self, value):
        # Takes 0 to 4095 integer and suppress with Gamma correction method.
        return  math.floor(pow(value/4095, self.GAMMACOEF) * 4095)

    # 数字が何かあったら、unsigned short でパックして、
    # char のタプルにする。
    def _convertTo24(self, arr):
        bytes24 = bytearray(self.NUM_TLCS*24)
        # 正規化
        for channel, H in enumerate(arr):
            H = max(0, min(H,4095)) # 何らかの不正データ対策
            index8 = (self.NUM_TLCS*16-1) - channel
            index12 = (index8*3)>>1
            # print("index12:", index12)
            if (index8 & 1):
                # print( index8, index12,1)
                bytes24[index12] =  (bytes24[index12] & 0xF0) | (H >> 8)
                bytes24[index12+1] = H & 0xFF
            else:
                # print( index8, 0)
                bytes24[index12]  = H >> 4
                # H は python int 型なので、最後の 1byte を取りだすのに struct.pack が必要
                bytes24[index12+1] = pack("H",((H & 0x00FF) << 4) | (bytes24[index12+1] & 0xF ))[0]
        return bytes24
    # bytes24 を一気に送る.

    # DC 用のバイト
    def convertTo12(self,arr):
        bytes12 = bytearray(self.NUM_TLCS*12)
        ran = range(0,self.NUM_TLCS*4)
        ran.reverse()
        for x in ran:
            i = x*4+3
            j = x*3+2
            bytes12[j] =  pack("H", arr[i] << 2 | (arr[i-1] >> 4))[0]
            bytes12[j-1]=  pack("H",(arr[i-1] << 4) | (arr[i-2] >> 2))[0]
            bytes12[j-2]=  pack("H",(arr[i-2] << 6) | arr[i-3])[0]
        return bytes12

    def fade(self, interval=0.02 ,speed=0.01, length=10):
        speed = min(self.FADESPEEDLIMIT, speed)
        radian = 0
        sum=0
        self.ser.flushInput() # flush "what"
        for  i in range(1,int(length/interval)) :
            if  (radian+math.pi/2) % (2*math.pi) < math.pi :
                radian = radian + interval*speed # interval が変わっても周期がかわらないように。
            else:
                radian = radian + interval*speed*3 # interval が変わっても周期がかわらないように。

            setval = int(2047*(math.sin(radian) + 1))
            #setAll(val)
            for j in range(self.NUM_TLCS*16):
                if j%4 == 0:
                    setval = int(2047*(math.sin(radian+(j%40)*0.1) + 1))
                if j%4 == 1:
                    setval = int(2047*(math.sin(radian+(j%40)*0.1 + math.pi/6) + 1))
                if j%4 == 2:
                    setval = int(2047*(math.sin(radian+(j%40)*0.1 + 2*math.pi/6) + 1))
                if j%4 == 0:
                    setval = int(2047*(math.sin(radian+(j%40)*0.1) + 1))
                self.setOne(j, setval)
            #setOne(arr, 14, val)
            start_time = time.time()

            self.updateDevice()
            
            sum = sum+(time.time() - start_time)
            #print(str(i).zfill(4), "val:", str(setval).zfill(5) ,"RTT:", "{:3.1f}(ms)".format(time.time()-start_time) ,"FPS:", "{:3.1f}".format((time.time() - start_time)*1000), resl)
            time.sleep(interval)
                
        #self.setAll(0)
        return 1

    def tlc_angleToVal(self, channel, angle):
        MIN_ANGLE=0
        MAX_ANGLE=4095
        sChannel = int((channel-3)/4)
        #print( sChannel, self.sfixMax[sChannel], self.sfixMin[sChannel])
        return int(4095 - self.sfixMax[sChannel] + angle*(self.sfixMax[sChannel] - self.sfixMin[sChannel])/ MAX_ANGLE);


    def unitTest(self, unit):
        # ユニットテスト
        self.setAll(0)
        sum=0
        start_time = time.time()

        # Delay shouldn't be zero, otherwise it causes data loss.
        delay = 0
        if unit % 4 == 3:
            delay = 0.03
        else:
            delay = 0.005 # Faster, but needs 5msec delay for robust communication
        for i in range(0,128):
            try:
                setval = int(2047*(-math.cos((32*math.pi/128)*i) + 1))
                self.setOne(unit, setval)
                serialStart = time.time()
                resl = self.updateDevice();
                RTT = (time.time() - serialStart)*1000
                print(str(i).zfill(4), "val:", str(setval).zfill(5) ,"RTT:", "{:3.1f}(ms)".format(RTT) ,"FPS:", "{:3.1f}".format(i/(time.time() - start_time)), resl)
            
                time.sleep(delay)
            except KeyboardInterrupt:
                self.ser.flushInput() # flush "what"
                break
        self.setAll(0)
        self.looseAll()
        return True
     