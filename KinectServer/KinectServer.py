from pykinect import nui
import numpy, socket, json, kserver

def video_frame_ready( frame ):
    pass
    
def depth_frame_ready( frame ):
    depth = numpy.empty( ( 240, 320, 1 ), numpy.uint16 )
    frame.image.copy_bits( depth.ctypes.data )
    global server
    print("depth:" + str(depth[120,160]))
    sdata = json.dumps({'depth': str(depth[120,160])})
    server.jsondata = sdata

if __name__ == '__main__':
    kinect = nui.Runtime()
    
    kinect.video_frame_ready += video_frame_ready
    kinect.depth_frame_ready += depth_frame_ready
    
    kinect.video_stream.open( nui.ImageStreamType.Video, 2, nui.ImageResolution.Resolution640x480, nui.ImageType.Color )
    kinect.depth_stream.open( nui.ImageStreamType.Depth, 2, nui.ImageResolution.Resolution320x240, nui.ImageType.Depth )
    
    # running server
    server = kserver.KServer(('127.0.0.1', 13374), kserver.KServerHandler)
    while True:
        try:
            server.handle_request()
        except KeyboardInterrupt:
            print("Confirm closing kinect")
            kinect.close()
            break
    
