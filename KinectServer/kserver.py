import SocketServer
import json, numpy

class KServer(SocketServer.ThreadingTCPServer):
    allow_reuse_address = True
    def __init__(self, server_address, RequestHandlerClass, bind_and_activate = True):
        self.jsondata = json.dumps({'no':'data'})
        self.timeout = 1
        return SocketServer.ThreadingTCPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate)


class KServerHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        try:
            data = json.loads(self.request.recv(1024).strip())
            # process the data, i.e. print it:
            
            print self.server.jsondata
            # send some 'ok' back
            self.request.sendall(self.server.jsondata)
        except Exception, e:
            print "Exception wile receiving message: ", e
