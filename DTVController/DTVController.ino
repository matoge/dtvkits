#include "Tlc5940.h"
#include "tlc_servos.h"

#define SERVO_CHANNEL   15
#define DELAY_TIME      50
#define BAUD_RATE 500000
 
#define POWER_OK 0
#define POWER_NOT_OK 1

#define TIMEDOUT 2
#define AVAILABLE 3

int count = 0;
int isReady= POWER_NOT_OK; // 初期化したかのフラグ。電源をコントロールする際に使用する。

void setup()
{
	isReady= POWER_NOT_OK;

	for( int i=3; i<14; i++){
		// At lieast 6 and 7 should be an output mode for power condrol
		// TLC pin should be pulled down.
		//do not poke pin 1 and 2, otherwise null charactor is sent to serial line
		digitalWrite(i,LOW);
		pinMode(i, OUTPUT); // pull down all the signals.
	}

	Serial.begin(BAUD_RATE);
	Serial.println("SERIALOK");

	// ここで、パワーソースが来る前に明示的にＰＣからの指示を仰ぐ
	while(isReady!=POWER_OK){ // 電源が入った)
		comPC();
	}

	
	myTLCInit();
}

/* TLC5940 Initialization Process*/
void myTLCInit(){
	tlc_initServos();  // Note: this will drop the PWM freqency down to 50Hz.
	Tlc.setAllDC(0);
	// サーボの初期値はぜろ（フリー
	Tlc.setAll(0);
	Tlc.update();
	delay(120);
	// 通信を開始
	//Serial.flush();

	for( count=0; count < NUM_TLCS*16; count++) {
			if ( count%4 == 3 )
				Tlc.setDC( count, 16);
			else
				Tlc.setDC( count,63);
		delay(4);               // wait for a second
	}
	Tlc.updateDC();

	// Blink all LEDs, leaving servo idle
	for( count=0; count< 3; count++) {
		for( int c2=0; c2 < NUM_TLCS*16; c2++) {
			Tlc.set( c2 , 0 );
		}
		Tlc.update();
		delay(120);
		for( int c2=0; c2 < NUM_TLCS*16; c2++) {
			if ( c2%4 == 3 )
				Tlc.set( c2 , 0 );
			else
				Tlc.set( c2,128);
		}
		Tlc.update();
		delay(120);
	}
	
	Serial.println("POWERON");
}

void reset(){
	delay(10);   
	Tlc.setAll(0);
	Tlc.update();
	delay(1000);   
	Serial.println("RESETDONE");
	software_Reset();  //call reset
	Serial.println("never happen");
}

void checkReset(){
	if( analogRead(0) < 500){
		reset();
	}
}

void software_Reset() // Restarts program from beginning but does not reset the peripherals and registers
{
	//SPI を切ってから全部ゼロにする。
	SPCR &= ~_BV(SPE);    // Disable SPI
	for( int i=0; i<14; i++){
		digitalWrite(i,LOW);
		pinMode(i, OUTPUT); // プルダウン・・・
		digitalWrite(i,LOW);
		// INPUT 
	}
	asm volatile ("  jmp 0");  
}  


void procSerial(){
	if( Serial.available() > 0){

		unsigned long start=millis();
		for( int i=0; i<NUM_TLCS*24; i++){
			while( Serial.available() <= 0){
			}
			tlc_GSData[i] = Serial.read();
			//Serial.print(tlc_GSData[i]); //debug
			if( millis()-start > 100){
				Serial.flush();
				Serial.println("x");
				return;
			}  
		}
		Tlc.update();
		Serial.flush();
		// update されたかどうかまだわからないが，
		// update() で tlc_needXLAT をチェックしているので気にせず次のデータを要求
		//Serial.flush();
		//Tlc.updateSerial(&Serial);
		Serial.println("f");
		//} else if ( command = "e" ){

		//}
	}

}

void waitPower(){
	/* Now waiting for the power source. Blink 13 until power on*/
	int state = HIGH;
	while( analogRead(0) < 500){
		delay(30);
		//digitalWrite(13, state);
		state = !state;
	}
	digitalWrite(13, LOW);
	digitalWrite(6,HIGH);
	digitalWrite(7,HIGH);
	isReady=POWER_OK;
	// confirm IC is powered on
	delay(30);
}

int waitPC(){
	unsigned long start=millis();
	while( Serial.available()<=0 ){
		if(millis()-start > 100){
			Serial.println("TIMEDOUT");
			return TIMEDOUT;
		}
	} // Should time out
	return AVAILABLE;
}

int comPC(){
	// コマンドの読み込みは共通なので、初期化、データの更新等はここに書く。
	// ハンドシェイクで行い、まずコマンドを受付け、そのコマンドに応じた「了承コマンド」を送る。
	// その後データを受け付ける。時間内に来なければタイムアウト
	if( Serial.available() >0 ){
		char b = Serial.read();
		if ( b == ':' ){
			String command=Serial.readStringUntil(';');

			if ( command.equals("send") && isReady == POWER_OK ){
				Serial.println("sendok");
				if ( waitPC() == AVAILABLE )
					procSerial();  
			}else if ( command.equals("init") && isReady == POWER_NOT_OK){ // 信号がHIGHの状態で電源を入れるとTLC5940が壊れる！
				Serial.println("initok");
				if ( waitPC() == AVAILABLE ){
					b = Serial.read();
					if (b==':'){
						// ここまで来たら、パワーを待つモード入る
						// :rest; -> restok -> : の処理を行わない限り電源は入らない。
						Serial.println("WAITPOWERON");
						waitPower();
					}
				}
			}else if ( command.equals("reset")){
				// ここはデータを読む必要はないので、来たらすぐにリセット
				reset();
			}else{
				Serial.println(command);
			}
		}else{
			// コマンドが不正の時はすべてすてる
			Serial.println("notcommand");
			Serial.flush();
		}
	}
}

void loop()
{
	checkReset();
	comPC();
}





