# Declare global variables here

import bpy,gVals,Panel, SocketOperator, StopOperator
import EditFunctions as ed

# Define global types 

from bpy.props import BoolProperty
from bpy.props import FloatProperty

bpy.types.Scene.isSendData = BoolProperty(
        name = "Send Socket", 
        description = "True or False?")

bpy.types.Scene.randomness = FloatProperty(
    name="Randomness",
    description="Float Value"
)

bpy.types.Scene.blendAlpha = FloatProperty(
    name="blendAlpha",
    description="Alpha Blending"
)

bpy.types.Scene.blendBeta = FloatProperty(
    name="blendBeta",
    description="Beta Blending"
)
# Registor operators
ed.addRegulation()
SocketOperator.register()
Panel.register()