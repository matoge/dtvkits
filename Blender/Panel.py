import bpy, gVals, SocketOperator, StopOperator
from bpy.props import BoolProperty

class NiPanel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_idname = "OBJECT_DTV"
    bl_label = "DepthTV"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "Tools"
    
    def draw(self, context):
        layout = self.layout
        obj = context.object

        #row = layout.row()
        #row.label(text="Hello world!", icon='WORLD_DATA')
        row = layout.row()
        row.label(text="Active object is: " + str(obj.name))
        
        if gVals.gisModalRunning == False:
            row= layout.row()
            row.operator("wm.modal_timer_socket")
        else:
            row= layout.row()
            row.operator("wm.stop_operator")
        
    
        row= layout.row()
        row.operator("wm.copy_material_to_selected")
        row= layout.row()
        row.operator("wm.copy_ylocation_to_selected")
        #if context.scene["op_prop"] == 1:
        #    row.enabled = False
        sce = context.scene
        row= layout.row()
# draw the checkbox (implied from property type = bool)
        row.prop(sce, "isSendData") 
        row= layout.row()
        row.prop(sce, "randomness")
        row= layout.row()
        row.operator("wm.smooth_adjacents")
        row= layout.row()
        row.prop(sce, "blendAlpha")
        row= layout.row()
        row.prop(sce, "blendBeta")
        row= layout.row()
        row.operator("wm.set_keyframes_in_group")
        row= layout.row()
        row.operator("wm.set_material_keyframes")
        #row = layout.row()
        #row.operator("wm.stop_operator")
        #if context.scene["op_prop"] == 0:
        #    row.enabled = False
        
        

def register():
    bpy.utils.register_class(NiPanel)


def unregister():
    bpy.utils.unregister_class(NiPanel)


if __name__ == "__main__":
    bpy.context.scene["op_prop"] = 0
    register()
    