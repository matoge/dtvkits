import bpy, math, gVals, DTVTCPClient
import socket, json
import colorsys
import EditFunctions as ed
import random, copy

import sys, os.path

class SocketOperator(bpy.types.Operator):
    """Operator which runs its self from a timer"""
    bl_idname = "wm.modal_timer_socket"
    bl_label = "Start Socket Operator"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    _socket = 0
    _count = 0
    _timer = None


    def modal(self, context, event):
        if event.type == 'ESC':
            context.scene["op_prop"] = 0
            return self.cancel(context)

        if event.type == 'TIMER':
            if gVals.gisModalRunning == False:
                self.cancel(context)
                return {'CANCELLED'}
            
            
            senddata = 400*[0]
            self._count += 1
            for j,item in enumerate(bpy.data.objects):
                #item.location.y = math.cos( (j+float(self._count))/20 )
                # hue 
                col = item.data.materials[0].diffuse_color 
                idx = int( item.name.split(".")[1])
                depth = item.location.y 
                depth = max(0, min(4, depth))
                senddata[idx*4+3] = int(depth*1023)
                #print( j, item.name, idx)
                # change color
                #hsv=colorsys.rgb_to_hsv(col[0], col[1], col[2])
                #hsv = ((hsv[0]+0.01)%1, hsv[1], hsv[2])
                #col = colorsys.hsv_to_rgb(hsv[0], hsv[1], hsv[2])
                #item.data.materials[0].diffuse_color = col
                senddata[idx*4] = int( 4095 *col[0] )
                senddata[idx*4+1] = int( 4095 *col[1] )
                senddata[idx*4+2] = int( 4095 *col[2] )
            #print(senddata)
            rel=DTVTCPClient.DTVSendData(senddata)
            #print(rel)
        return {'PASS_THROUGH'}

    def execute(self, context):
        # Initialize Socket
        # execute when not initialized 
        self._timer = context.window_manager.event_timer_add(0.03, context.window)
        context.window_manager.modal_handler_add(self)
        gVals.gisModalRunning = True
        ed.removeRegulation()
        return {'RUNNING_MODAL'}
        

    def cancel(self, context):
        context.window_manager.event_timer_remove(self._timer)
        ed.addRegulation()
        return {'CANCELLED'}
   
def CopyMaterialSingle(fromOb, toOb,rand):
    hsv = fromOb.material_slots[0].material.diffuse_color.hsv
    # add random to hue
    #hue = (hsv[0]+0.1)%1
    hue = (hsv[0] + random.uniform(-rand,rand))%1
    toOb.material_slots[0].material.diffuse_color.hsv = (hue,hsv[1],hsv[2])
    
class CopyMaterialToSelected(bpy.types.Operator):
    bl_idname = "wm.copy_material_to_selected"
    bl_label = "Copy Material To Selected"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    def execute(self, context):
        # concext.object
        active = context.object
        randomness = context.scene.randomness/100
        for ob in bpy.data.objects:
            if ob.select == True and ob!=active:
                CopyMaterialSingle(active,ob,randomness)
        return {'FINISHED'}


class CopyYLocationToSelected(bpy.types.Operator):
    bl_idname = "wm.copy_ylocation_to_selected"
    bl_label = "Copy Y Location To Selected"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    def execute(self, context):
        # concext.object
        active = context.object
        randomness = context.scene.randomness/100
        for ob in bpy.data.objects:
            if ob.select == True:
                ob.location.y = active.location.y
        return {'FINISHED'}


class SmoothAdjecents(bpy.types.Operator):
    """Smooth positions and materials of adjacent objects in selected group"""
    bl_idname = "wm.smooth_adjacents"
    bl_label = "Blend Adjacents"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    
    def blend(self,ob1, obs,alpha,beta):
        beta = (1-alpha)/len(obs)
        bloc = ob1.location.y*alpha
        for ob in obs:
            bloc=bloc  + ob.location.y*beta
        ob1.location.y = bloc
        #ob2.location.y = bloc    
        
        # color, mix in rgb
        hsva = ob1.material_slots[0].material.diffuse_color
        hsvc = ( hsva[0]*alpha, hsva[1]*alpha, hsva[2]*alpha)
        for ob in obs:
            hsvb = ob.material_slots[0].material.diffuse_color
            hsvc = ( hsvc[0]+hsvb[0]*beta,hsvc[1]+hsvb[1]*beta,hsva[2]+hsvb[2]*beta )
        ob1.material_slots[0].material.diffuse_color = hsvc
        #ob2.material_slots[0].material.diffuse_color.hsv = hsvc
        
    def execute(self, context):
        # concext.object
        # object is adjacent when it's index 
        # Collect selected objects
        selectedIdx = []
        for ob in bpy.data.objects:
            index = int(ob.name.split('.')[1])
            if ob.select == True:
                selectedIdx.append(index)
                
        for index in selectedIdx:
            idx1 = index + 10 # right
            idx2 = index + 1 # top
            idx3 = index - 1 # bottom
            idx4 = index - 10 # left
            # rondomly select adjacent 
            obs = []
            if ( idx1 in selectedIdx and idx1 <100 ):
                obs.append( bpy.data.objects[idx1] )
            if ( idx2 in selectedIdx and idx2%10 > index%10 ):
                obs.append( bpy.data.objects[idx2] )
            if ( idx3 in selectedIdx and idx3%10 < index%10):
                obs.append( bpy.data.objects[idx3] )
            if ( idx4 in selectedIdx and idx4 > -1):
                obs.append( bpy.data.objects[idx4] )
            
            #print( str(index) + ":" + str(len(obs)))
            if ( len(obs) > 1):
                self.blend( bpy.data.objects[index], obs , context.scene.blendAlpha, context.scene.blendBeta)
                
                # adjacent (left top) is 
        return {'FINISHED'}

    
class SetKeyframesInGroup(bpy.types.Operator):
    """Set keyframes of Location and Materials of selected objects"""
    bl_idname = "wm.set_keyframes_in_group"
    bl_label = "Set Keyframes Locations"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    def execute(self, context):
        # concext.object
        selectedIdx = []
        for ob in bpy.data.objects:
            index = int(ob.name.split('.')[1])
            if ob.select == True:
                ob.keyframe_insert(data_path='location')
                
        return {'FINISHED'}
    
class SetMaterialKeyframesInGroup(bpy.types.Operator):
    """Set keyframes of Location and Materials of selected objects"""
    bl_idname = "wm.set_material_keyframes"
    bl_label = "Set Keyframes Material"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    def execute(self, context):
        # concext.object
        selectedIdx = []
        for ob in bpy.data.objects:
            index = int(ob.name.split('.')[1])
            if ob.select == True:
                ob.material_slots[0].material.keyframe_insert(data_path='diffuse_color')
                
        return {'FINISHED'}



def register():
    bpy.utils.register_class(SocketOperator)
    bpy.utils.register_class(CopyMaterialToSelected)
    bpy.utils.register_class(CopyYLocationToSelected)
    bpy.utils.register_class(SmoothAdjecents)
    bpy.utils.register_class(SetKeyframesInGroup)
    bpy.utils.register_class(SetMaterialKeyframesInGroup)
    ed.addRegulation()


def unregister():
    bpy.utils.unregister_class(SocketOperator)
    bpy.utils.unregister_class(CopyMaterialToSelected)
    bpy.utils.unregister_class(CopyYLocationToSelected)
    bpy.utils.unregister_class(SmoothAdjecents)
    bpy.utils.unregister_class(SetKeyframesInGroup)
    bpy.utils.unregister_class(SetMaterialKeyframesInGroup)