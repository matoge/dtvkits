import bpy, math

def createWave():
    n=25
    m=4
    # Group 
    for i in range(0,n):
        for j in range(0,m):
            x = -5+math.floor(i/5)*2 + (j==2 or j==1 )
            y = (i%5)*2+math.floor(j/2)
            
            bpy.context.scene.frame_set(0)
            obj = bpy.context.scene.objects['Cube.'+str(i*4+j)]
            obj.keyframe_insert(data_path='location')
            bpy.context.scene.frame_set(10+ i*4+j%10)
            obj.location.y = 5
            obj.keyframe_insert(data_path='location')
            bpy.context.scene.frame_set(20+ i*4+j%10)
            obj.location.y = 0
            obj.keyframe_insert(data_path='location')

createWave()