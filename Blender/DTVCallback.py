import bpy  
import gVals
import time, math

def regulateLocations(context):
    if bpy.data.objects.is_updated:
        #print("One or more objects were updated!")
        for ob in bpy.data.objects:
            if ob.is_updated:
                #print("=> Servo Num:", ob.name.split("."), " ServoPos:", ob.ServoPos)
                #idx = int( ob.name.split('.')[1])
                # Restrict Positions
                depth = ob.location.y
                depth = max(0, min(4, depth))
                ob.location.y = depth
                
    if bpy.data.materials.is_updated:
        pass

# For debug. remove all functions            
try:
    for fun in bpy.app.handlers.scene_update_post:
        #if fun.__name__ == "scene_update":bpo
        print("function '"+ fun.__name__ +"' deleted")
except:
    print("no function, now function added")
    
bpy.app.handlers.scene_update_post.append(scene_update)
print("callback function 'scene_update' added")
#bpy.app.handlers.frame_change_pre.append(my_handler)