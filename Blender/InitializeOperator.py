import bpy
import serialTLC
import gVals

class InitializeOperator(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "wm.initialize_operator"
    bl_label = "Init Serial"

    def execute(self, context):
        try:
            gVals.gSerial1.initSerial("COM19",gVals.BAUDRATE)
        except:
            print(port + " not initialized, Now let it start.")
        return {'FINISHED'}

def register():
    bpy.utils.register_class(InitializeOperator)

def unregister():
    bpy.utils.unregister_class(InitializeOperator)


#if __name__ == "__main__":
register()

