import bpy
from random import random

# Delete All Objects
def deleteAll():
    for item in bpy.data.objects:
        item.select = True
    bpy.ops.object.delete()
    for mat in bpy.data.materials:
        bpy.data.materials.remove(mat)
    for mesh in bpy.data.meshes:
        bpy.data.meshes.remove(mesh)

def detelAllGroups():
    for g in bpy.data.groups:
        bpy.data.gropus.remove(g)

def redraw():
    bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP',iterations=1)

def makeCube(x,y,z):
    ob = bpy.ops.mesh.primitive_cube_add(location=(x,y,z))
    return ob

def initMirror(n,m):
    deleteAll()
    # Group 
    bpy.ops.group.create(name="Pixels")
    
    for i in range(0,n):
        for j in range(0,m):
            makeCube(i-5,0,j)
            ob = bpy.context.object
            # lock location in x and z directions
            ob.lock_location[0] = True
            ob.lock_location[2] = True
            
            scale_s = 0.45
            scale_l = 2
            ob.scale = (scale_s,scale_l,scale_s)
            ob.name = "Cube." + str(i*m+j).zfill(2)

            mat = bpy.data.materials.new('Cube')
            mat.name = "Mat." + str(i*m+j)
            mat.diffuse_color = ( i/n, j/m,0)
            mat.use_transparency = True
            mat.alpha = 0.6
            ob.data.materials.append(mat)
#            m.materials[0].rgbCol = [random(), random(), random()]
            
        # Add to group
            bpy.ops.object.group_link(group="Pixels")
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP',iterations=0)
# f

initMirror(10,10)
