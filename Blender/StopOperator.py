import bpy

def main(context):
    for ob in context.scene.objects:
        print(ob)

class StopOperator(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "wm.stop_operator"
    bl_label = "Stop Modal Operator"

    def execute(self, context):
        context.scene["op_prop"] = 0;
        return {'FINISHED'}


def register():
    bpy.utils.register_class(StopOperator)


def unregister():
    bpy.utils.unregister_class(StopOperator)


if __name__ == "__main__":
    register()

    # test call
    bpy.ops.wm.stop_operator()
