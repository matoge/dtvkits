﻿# coding:utf-8

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import serialTLC
from listserial import serial_ports

class Form(QWidget):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

#-------シリアル
        self.s = serialTLC.serialTLC()

#-------部品: Initialiation

        nameLabel = QLabel("Name:")
        self.nameLine = QLineEdit()
        self.resetButton = QPushButton("Init/Reset Serial")
        self.finalizeButton = QPushButton("Finalize Serial")
        self.zeroButton = QPushButton("Set Zero to All")
        self.combo = QComboBox()

        self.resetButton.clicked.connect(self.submitContact)
        self.finalizeButton.clicked.connect(self.finalizeSerial)
        self.zeroButton.clicked.connect(self.setAllzero)

#-------部品: UnitTest
        self.unitspin = QSpinBox()
        self.unitTestButton = QPushButton("Start Unit Test")
        self.unitTestButton.clicked.connect(self.unitTest)
#-------部品: SequenceTest
        self.sqspin = QSpinBox()
        self.sqTestButton = QPushButton("Start Sequence Test")
        self.sqTestButton.clicked.connect(self.sqTest)
#-------部品: SWhole Test
        self.wlspin = QSpinBox()
        self.wlTestButton = QPushButton("Start Sequence Test")
        self.wlTestButton.clicked.connect(self.wlTest)

#-------レイアウト: Initalization
        buttonLayout1 = QVBoxLayout()
        group1 = QGroupBox("Initialization")
        group1Layout = QVBoxLayout()
        group1Layout.addWidget(self.combo)
        group1Layout.addWidget(self.resetButton)
        group1Layout.addWidget(self.finalizeButton)
        group1Layout.addWidget(self.zeroButton)
        group1.setLayout(group1Layout)
        buttonLayout1.addWidget(group1)

        # リストはマニュアルで
        self.combo.addItem("COM19")
        self.combo.addItem("COM22")
        self.combo.addItem("COM23")

#-------レイアウト：unittest

        buttonLayout2 = QVBoxLayout()
        group2 = QGroupBox("Unit Test")
        group2Layout = QVBoxLayout()
        group2Layout.addWidget(self.unitspin)
        group2Layout.addWidget(self.unitTestButton)
        group2.setLayout(group2Layout)
        buttonLayout2.addWidget(group2)

#-------レイアウト：sequence test

        buttonLayoutSq = QVBoxLayout()
        groupSq = QGroupBox("Sequence Test")
        groupSqLayout = QVBoxLayout()
        groupSqLayout.addWidget(self.sqspin)
        groupSqLayout.addWidget(self.sqTestButton)
        groupSq.setLayout(groupSqLayout)
        buttonLayoutSq.addWidget(groupSq)
#-------レイアウト：whole test

        buttonLayoutWl = QVBoxLayout()
        groupWl = QGroupBox("Exhausive Test")
        groupWlLayout = QVBoxLayout()
        groupWlLayout.addWidget(self.wlspin)
        groupWlLayout.addWidget(self.wlTestButton)
        groupWl.setLayout(groupWlLayout)
        buttonLayoutWl.addWidget(groupWl)


#-------メインレイアウト
        mainLayout = QGridLayout()

        # mainLayout.addWidget(nameLabel, 0, 0)
        mainLayout.addLayout(buttonLayout1, 0, 0)
        mainLayout.addLayout(buttonLayout2, 1, 0)
        mainLayout.addLayout(buttonLayoutSq, 2, 0)
        mainLayout.addLayout(buttonLayoutWl, 3, 0)


        self.setLayout(mainLayout)
        self.setWindowTitle("Confirm Reset")

    def submitContact(self):
        name = self.combo.currentText()
        if name == "":
            QMessageBox.information(self, "Empty Field","Please enter a name and address.")
            return
        else:
            QMessageBox.information(self, "Success!","Open port \"%s\"!" % name)
            self.resetSerial(name)


    def resetSerial(self, port):
        try:
            self.s.finalize()
        except:
            print("Not initialized, start")
        print(self.s.initSerial(port,256000))

    def finalizeSerial(self, port):
        try:
            self.s.finalize()
        except:
            print("Not initialized, start")
        self.s.initSerial(port,256000)

    def setAll(self,value):
        self.s.setAll(value)

    def setAllzero(self):
        self.setAll(0)

    def unitTest(self):
        self.s.unitTest(self.unitspin.value())

    def sqTest(self):
        self.s.unitTest(self.unitspin.value())

    def wlTest(self):
        for i in range(1,10):
            self.s.unitTest(self.unitspin.value())

if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    screen = Form()
    screen.show()

    sys.exit(app.exec_())