# coding:utf-8
import threading
import sys, time

class hoge:
    def __init__(self,x1):
        self.x = x1

def worker(hoge):
    """thread worker function"""
    print('Worker: %s'% hoge.x)
    return

def ThreadTest():
    threads = []
    for i in range(5):
        t = threading.Thread(target=worker, args=(hoge(i),))
        threads.append(t)
        t.start()

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class Form(QWidget):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        mainLayout = QGridLayout()

        self.setLayout(mainLayout)
        self.setWindowTitle("Confirm Reset")

        self.objThread = QThread()
        self.obj = MyObject()
        self.obj.moveToThread(self.objThread)
        self.obj.finished.connect(self.objThread.quit)
        self.objThread.started.connect(self.obj.longRunning)
        #self.objThread.finished.connect(app.exit)
        self.objThread.start()

class MyObject(QObject):

    finished = pyqtSignal()
    def longRunning(self):
        count = 0
        while count < 5:
            time.sleep(1)
            print ("Increasing")
            count += 1
        self.finished.emit()

#def usingMoveToThread():

if __name__ == "__main__":

    app = QApplication(sys.argv)
##

    screen = Form()
    screen.show()

    sys.exit(app.exec_())
    #usingQThread()
    #usingMoveToThread()
    #usingQRunnable()