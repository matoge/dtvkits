# coding:utf-8

# 一つの Arduino と通信するための ライブラリ。
# serial が必要.

import os
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

import serial, array, math, time
from struct import *


class serialTLC:
    def __init__(self):
        self.ser = 0
        self.NUM_TLCS=15
        self.arr = array.array('H') #unsigned short
        self.arr.fromlist([0]*self.NUM_TLCS*16) # 必要な数だけ。
        self.arrser = None # フィルタリング用
        self.FADESPEEDLIMIT = 8

    def initSerial(self,com,baud=200000):
        # DTS を Disableしない限り、Arduino はリセットされる。
        self.ser = serial.Serial(com, baud, timeout=1)
        if(self.ser.isOpen() == False):
            self.ser.close()
            self.ser = serial.Serial(com, baud, timeout=1)
        self.setAll(0)
        print("initialized")
        #self.ser.write(bytes("a",encoding="ascii"))
        #self.ser.flushInput()

    def setAll(self, val):
        #self.arr = array.array('H') #unsigned shortn
        for j in range(self.NUM_TLCS*16):
            if j%4 == 3:
                self.setOne(j, val)
            else:
                self.setOne(j, 4095-val)
        self.ser.write(self._convertTo24(self.arr))
        ret = self.ser.readline()
        time.sleep(0.1) # この後時間を置かないとエラーになる。
        print("return",ret) # Wait for reply
        #return arr

    # サーボの値をチェックして、おかしければ変える。
#    def normAll(self, val):

    def finalize(self):
        self.ser.close()

    def setOne(self, channel, val):
        # 現在の値
        self.arr[channel] = int(val)
        # self.ser.write(self._convertTo24(self.arr))
        # print("return",self.ser.readline()) # Wait for reply

    # 数字が何かあったら、unsigned short でパックして、
    # char のタプルにする。
    def _convertTo24(self, arr):
        bytes24 = bytearray(self.NUM_TLCS*24)
        for channel, H in enumerate(arr):
            index8 = (self.NUM_TLCS*16-1) - channel
            index12 = (index8*3)>>1
            # print("index12:", index12)
            if (index8 & 1):
                # print( index8, index12,1)
                bytes24[index12] =  (bytes24[index12] & 0xF0) | (H >> 8)
                bytes24[index12+1] = H & 0xFF
            else:
                # print( index8, 0)
                bytes24[index12]  = H >> 4
                # H は python int 型なので、最後の 1byte を取りだすのに struct.pack が必要
                bytes24[index12+1] = pack("H",((H & 0x00FF) << 4) | (bytes24[index12+1] & 0xF ))[0]
        return bytes24
    # bytes24 を一気に送る.

    # DC 用のバイト
    def convertTo12(self,arr):
        bytes12 = bytearray(self.NUM_TLCS*12)
        ran = range(0,self.NUM_TLCS*4)
        ran.reverse()
        for x in ran:
            i = x*4+3
            j = x*3+2
            bytes12[j] =  pack("H", arr[i] << 2 | (arr[i-1] >> 4))[0]
            bytes12[j-1]=  pack("H",(arr[i-1] << 4) | (arr[i-2] >> 2))[0]
            bytes12[j-2]=  pack("H",(arr[i-2] << 6) | arr[i-3])[0]
        return bytes12

    def fade(self, interval=0.1, speed=1, length=10):
        speed = min(self.FADESPEEDLIMIT, speed)
        radian = 0
        sum=0
        self.ser.flushInput() # flush "what"
        for  i in range(1,int(length/interval)) :
            if  (radian+math.pi/2) % (2*math.pi) < math.pi :
                radian = radian + interval*speed # interval が変わっても周期がかわらないように。
            else:
                radian = radian + interval*speed*4 # interval が変わっても周期がかわらないように。

            setval = int(2047*(math.sin(radian) + 1))
            #setAll(val)
            for j in range(self.NUM_TLCS*16):
                setval = int(2047*(math.sin(radian+j*0.4+j%4) + 1))
                if j%4 ==3:
                    self.setOne(j, self.tlc_angleToVal(setval))
                else:
                    self.setOne(j, setval)
            #setOne(arr, 14, val)
            start_time = time.time()
            self.ser.readline()
            self.ser.write(self._convertTo24(self.arr))
            resl = self.ser.readline().strip() # Wait for reply
            sum = sum+(time.time() - start_time)
            print(str(i).zfill(4), "val:", str(setval).zfill(5) ,"RTT:", "{:3.1f}(ms)".format((time.time() - start_time)*1000), resl)
            #sum = sum + time.time() - start_time
            time.sleep(interval)

        self.setAll(0)

    def tlc_angleToVal(self, angle):
        MIN_WIDTH=120
        MAX_WIDTH=500
        MIN_ANGLE=0
        MAX_ANGLE=4095
        return 4095 - MAX_WIDTH + angle*(MAX_WIDTH - MIN_WIDTH)/ MAX_ANGLE;

    def setAllslow():
        # array の array, arrser を登録し、再生要求があれば
        # arrser から pop して set する。
        return True

    def unitTest(self, unit):
        # ユニットテスト
        self.setAll(0)
        sum=0
        start_time = time.time()
        #self.ser.flushInput() # flush "what"
        for i in range(0,128):
            try:
                setval = int(2047*(-math.cos((4*math.pi/128)*i) + 1))
                if unit%4 ==3:
                    #for j in range(0,16):
                    self.setOne(unit, self.tlc_angleToVal(setval))
                else:
                    self.setOne(unit, setval)
                    #for j in range(0,16):
                    self.setOne(unit, setval)
                self.ser.readline()
                self.ser.write(self._convertTo24(self.arr))
                resl = self.ser.readline().strip() # Wait for reply
                sum = sum+(time.time() - start_time)
                print(str(i).zfill(4), "val:", str(setval).zfill(5) ,"RTT:", "{:3.1f}(ms)".format((time.time() - start_time)*1000), resl)
                time.sleep(0.02)
            except KeyboardInterrupt:
                self.ser.flushInput() # flush "what"
                break
        self.setAll(0)
        return True

    def heartbeat(self, interval=0.1, speed=1, length=10):
        speed = min(self.FADESPEEDLIMIT, speed)
        radian = 0
        sum=0
        self.ser.flushInput() # flush "what"
        for  i in range(1,int(length/interval)) :
            if  (radian+math.pi/2) % (2*math.pi) < math.pi :
                radian = radian + interval*speed # interval が変わっても周期がかわらないように。
            else:
                radian = radian + interval*speed*4 # interval が変わっても周期がかわらないように。

            setval = int(2047*(math.sin(radian) + 1))
            for j in range(self.NUM_TLCS*16):
                #setval = int(2047*(math.sin(radian+j*0.4+j%4) + 1))
                setval = int(2047*(math.sin(radian) + 1))
                if j%4 ==3:
                    self.setOne(j, self.tlc_angleToVal(setval))
                else:
                    self.setOne(j, setval)

            #setOne(arr, 14, val)
            start_time = time.time()
            self.ser.readline()
            self.ser.write(self._convertTo24(self.arr))
            resl = self.ser.readline().strip() # Wait for reply
            sum = sum+(time.time() - start_time)
            print(str(i).zfill(4), "val:", str(setval).zfill(5) ,"RTT:", "{:3.1f}(ms)".format((time.time() - start_time)*1000), resl)
            #sum = sum + time.time() - start_time
            time.sleep(interval)

        self.setAll(0)


